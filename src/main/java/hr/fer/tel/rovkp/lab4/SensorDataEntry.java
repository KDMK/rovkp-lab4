package hr.fer.tel.rovkp.lab4;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class SensorDataEntry implements Comparable<SensorDataEntry> {

    private long stationID; // 0
    private LocalDateTime createdAt; // 1-6
    private long timestamp; // 7
    private long sequenceNumber; // 8
    private double configSamplingTime; // 9
    private double dataSamplingTime;
    private double radioDutyCycle;
    private double radioTransmissionPower;
    private double radioTransmissionFrequency;
    private double primaryBufferVoltage;
    private double secondaryBufferVoltage;
    private double solarPanelCurrent;
    private double globalCurrent;
    private double energySource;

    public static SensorDataEntry parseInputString(String input) {
        String[] parts = input.split("\\s");
        if (parts.length != 19) {
            throw new IllegalArgumentException("Cannot parse input line, incorrect number of arguments");
        }

        SensorDataEntry entry = new SensorDataEntry();
        entry.stationID = Long.parseLong(parts[0]);

        // convert data fields into LocalDateTime
        int year = Integer.parseInt(parts[1]);
        int month = Integer.parseInt(parts[2]);
        int day = Integer.parseInt(parts[3]);
        int hour = Integer.parseInt(parts[4]);
        int min = Integer.parseInt(parts[5]);
        int sec = Integer.parseInt(parts[6]);

        entry.createdAt = LocalDateTime.of(year, month, day, hour, min, sec);
        entry.timestamp = Long.parseLong(parts[7]);
        entry.sequenceNumber = Long.parseLong(parts[8]);

        entry.configSamplingTime = Double.parseDouble(parts[9]);
        entry.dataSamplingTime = Double.parseDouble(parts[10]);
        entry.radioDutyCycle = Double.parseDouble(parts[11]);
        entry.radioTransmissionPower = Double.parseDouble(parts[12]);
        entry.radioTransmissionFrequency = Double.parseDouble(parts[13]);
        entry.primaryBufferVoltage = Double.parseDouble(parts[14]);
        entry.secondaryBufferVoltage = Double.parseDouble(parts[15]);
        entry.solarPanelCurrent = Double.parseDouble(parts[16]);
        entry.globalCurrent = Double.parseDouble(parts[17]);
        entry.energySource = Double.parseDouble(parts[18]);

        return entry;
    }


    @Override
    public int compareTo(SensorDataEntry other) {
        return this.createdAt.compareTo(other.getCreatedAt());
    }

    @Override
    public String toString() {
        return String.format("%d,%02d,%02d,%02d,%02d,%02d,%02d,%d,%d,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f",
                stationID,
                createdAt.getYear(),
                createdAt.getMonthValue(),
                createdAt.getDayOfMonth(),
                createdAt.getHour(),
                createdAt.getMinute(),
                createdAt.getSecond(),
                timestamp,
                sequenceNumber,
                configSamplingTime,
                dataSamplingTime,
                radioDutyCycle,
                radioTransmissionPower,
                radioTransmissionFrequency,
                primaryBufferVoltage,
                secondaryBufferVoltage,
                solarPanelCurrent,
                globalCurrent,
                energySource
        );
    }
}
