package hr.fer.tel.rovkp.lab4;


import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * Hello world!
 */
public class App {

    private static final Pattern INPUT_PATTERN = Pattern.compile("sensorscope-monitor-[0-9]+\\.txt");
    private static final Logger log = getLogger(App.class);

    public static void main(String[] args) throws IOException {
        Path resourcesDir = Paths.get("/home/mbartolac/Workspaces/java/ROVKP-lab4/src/main/resources/sensorscope");
        String[] files = resourcesDir.toFile().list();
        log.info("Started reading files from {} with path {}", resourcesDir, INPUT_PATTERN.toString());
        assert files != null;
        List<String> inputFiles = Stream.of(files)
                .filter(t -> t.matches(INPUT_PATTERN.toString()))
                .map(t -> String.format("%s/%s", resourcesDir.toString(), t))
                .collect(Collectors.toList());

        List<String> lines = new ArrayList<>();
        for(String inputFile: inputFiles) {
            log.info("Appending {}", inputFile);
            lines.addAll(Files.readAllLines(Paths.get(inputFile)));
        }
        log.info("Finished reading files content");

        log.info("Started sorting and filtering input entries");
        List<SensorDataEntry> entries = lines.stream()
                .map(SensorDataEntry::parseInputString)
                .sorted(SensorDataEntry::compareTo)
                .collect(Collectors.toList());
        log.info("Finished sorting and filtering input entries");

//        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        Path outputPath = Paths.get("/home/mbartolac/Workspaces/java/ROVKP-lab4/src/main/resources/sensorscope.csv");
        BufferedWriter bufferedWriter = Files.newBufferedWriter(outputPath);
        log.info("Writing entries to file {}", outputPath.toString());
        for(SensorDataEntry e: entries) {
            bufferedWriter.write(e.toString());
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
        bufferedWriter.close();
        log.info("Done!");
    }

    // Koliko je bilo ulaznih datoteka? 97
    // Koliko je bilo ulaznih zapisa? 4 726 643
    // Koliko je velika izlazna datoteka? 458 MB
}
