package hr.fer.tel.rovkp.lab4;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Spark1 {

    private static void main(String[] args) {
        SparkConf conf = new SparkConf().setAppName("Task2 hw");
        //set the master if not already set through the command line

        try {
            conf.get("spark.master");
        } catch (NoSuchElementException ex) {
            conf.setMaster("local");
        }

        JavaSparkContext sc = new JavaSparkContext(conf);

        //crate an RDD from text file lines
        JavaRDD<String> lines = sc.textFile("/home/mbartolac/Workspaces/java/ROVKP-lab4/src/main/resources/StateNames.csv");

        //do the job
        JavaRDD<SensorDataEntry> result = lines
                .map(SensorDataEntry::parseInputString);

        //write results to a file

        result.saveAsTextFile(args[1]);
    }
}
